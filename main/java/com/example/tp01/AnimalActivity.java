package com.example.tp01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View;
public class AnimalActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);



        final TextView textview = (TextView) findViewById(R.id.nom);
        ImageView m = (ImageView)findViewById(R.id.img);
        final TextView tv1 = (TextView)findViewById(R.id.textView7);
        final TextView tv2 = (TextView)findViewById(R.id.textView8);
        final TextView tv3 = (TextView)findViewById(R.id.textView9);
        final EditText ed4 = (EditText)findViewById(R.id.editText);
        final TextView tv5 = (TextView)findViewById(R.id.textView10);
        final Intent main=new Intent(this,MainActivity.class);
        Button btn = (Button) findViewById(R.id.btn);



        Intent intent = getIntent();
        Bundle bnd = intent.getBundleExtra("key");
        final String animal_name = bnd.getString("cnt");

        textview.setText(animal_name);


        AnimalList cl = new AnimalList();
        final Animal  n = cl.getAnimal(animal_name);


        int id=getResources().getIdentifier("com.example.tp01:drawable/"+n.getImgFile(),"null","null")  ;
        m.setImageResource(id);

        final String vm=n.getStrHightestLifespan();
        tv2.setText(vm);

        final String pg=n.getStrGestationPeriod();
        tv3.setText(pg);

        final String pn=n.getStrBirthWeight();
        tv1.setText(pn);


        final String pa=n.getStrAdultWeight();
        tv5.setText(pa);

        final String sc = n.getConservationStatus();
        ed4.setText(sc);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String ss=String.valueOf(ed4.getText());
                n.setConservationStatus(String.valueOf(ss));
                startActivity(main);


            }
        });
    }
}
