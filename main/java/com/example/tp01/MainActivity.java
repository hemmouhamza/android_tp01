package com.example.tp01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AnimalList a = new AnimalList();
        String[] values = a.getNameArray();
        final ListView listview = (ListView) findViewById(R.id.liste);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
        listview.setAdapter(adapter);

        final Intent intent = new Intent(this, AnimalActivity.class);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final String item = (String) parent.getItemAtPosition(position);
                Bundle bn = new Bundle();
                bn.putString("cnt", item);
                intent.putExtra("key", bn);
                startActivity(intent);
            }
        });
    }
}
